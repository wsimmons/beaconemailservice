﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Http;
using System.Text;

namespace WebApiTest.Controllers
{
    public class EmailController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="mmsId"></param>
        /// <param name="scheduledTime"></param>
        /// <param name="cancellationTime"></param>
        /// <param name="cancellationReason"></param>
        /// <param name="comment"></param>
        /// <param name="reasonId"></param>
        /// <returns></returns>
        [ActionName("cancellation")]
        [HttpGet]
        public string SendCancellationEmail(string username, string mmsId, string scheduledTime, string cancellationTime, string cancellationReason,
            string comment, string reasonId)
        {
            string emailBody = CreateCancellationEmailBody(username, mmsId, scheduledTime, cancellationTime, cancellationReason, comment, reasonId);
            return Send(username, emailBody, NotificationTypeEnum.Cancellation);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="mmsId"></param>
        /// <param name="scheduledTime"></param>
        /// <param name="cancellationTime"></param>
        /// <param name="cancellationReason"></param>
        /// <param name="comment"></param>
        /// <param name="reasonId"></param>
        /// <returns></returns>
        private string CreateCancellationEmailBody(string username, string mmsId, string scheduledTime, string cancellationTime, string cancellationReason, string comment, string reasonId)
        {
            var bodyBuilder = new StringBuilder();
            bodyBuilder.AppendLine($"User: {username}");
            bodyBuilder.AppendLine($"MMSID: {mmsId}");
            bodyBuilder.AppendLine($"Scheduled Time: {scheduledTime}");
            bodyBuilder.AppendLine($"Cancellation Time: {cancellationTime}");
            bodyBuilder.AppendLine($"Cancellation Reason: {cancellationReason} ({reasonId})");
            if (!string.IsNullOrWhiteSpace(comment))
                bodyBuilder.AppendLine($"Comment: {comment}");
            return bodyBuilder.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fullname"></param>
        /// <param name="role"></param>
        /// <param name="total"></param>
        /// <param name="current"></param>
        /// <returns></returns>
        [ActionName("pto")]
        [HttpGet]
        public string SendPtoEmail(string fullname, string role, string total, string current)
        {
            string emailBody = CreatePtoEmailBody(fullname, role, total, current);
            return Send(fullname, emailBody, NotificationTypeEnum.PTO);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fullname"></param>
        /// <param name="role"></param>
        /// <param name="total"></param>
        /// <param name="current"></param>
        /// <returns></returns>
        private string CreatePtoEmailBody(string fullname, string role, string total, string current)
        {
            var bodyBuilder = new StringBuilder();
            bodyBuilder.AppendLine($"User: {fullname}");
            bodyBuilder.AppendLine($"Role: {role}");
            bodyBuilder.AppendLine($"Total PTO: {total}");
            bodyBuilder.AppendLine($"Current PTO: {current}");
            return bodyBuilder.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="emailBody"></param>
        /// <param name="notificationType"></param>
        /// <returns></returns>
        private string Send(string name, string emailBody, NotificationTypeEnum notificationType)
        {
            try
            {
                using (var client = new SmtpClient())
                {
                    client.Send(
                        ConfigurationManager.AppSettings["mailFrom"],
                        ConfigurationManager.AppSettings["mailRecipients"],
                        notificationType == NotificationTypeEnum.Cancellation ?
                        $"Beacon Skinny API Test - Provider Cancel Notification by {name}": $"Beacon Skinny API Test - PTO Notification for {name}",
                        emailBody);
                }
                return $"{notificationType} Notification Sent Successfully!";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private enum NotificationTypeEnum
        {
            Cancellation,
            PTO
        }
    }
}
